package require_var

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
)

type VarDef struct {
	Name        string
	Description string
	FileSuffix  string
}

func (vd *VarDef) HasFileSuffixSupport() bool {
	return vd.FileSuffix != ""
}

// Takes a variable definition and checks if the current environment meets the defined constraints.
// The order of precedence is:
// 		1. variable which name is given in the VarDef instance
//		2. suffixed version of the variable (if it is declared to be suffixed)
// If no value could be found, the function produces an informative error. If the variable definition
// contains a description field, it will be printed to help the client developer to understand the
// error.
// If a suffixed version is found, the variable content will be loaded using the value of the suffixed-variable
// and filled in the canonical variable.
func RequireVar(vd *VarDef) (string, error) {
	if vd == nil {
		return "", errors.New("cannot require nil VarDef")
	}
	if vd.Name == "" {
		return "", errors.New("invalid VarDef, no Name provided")
	}
	_, found := os.LookupEnv(vd.Name)

	// First, search for a value for the canonical variable
	if !found {
		// If no value is available and the variable doesn't support a suffix: error
		if !vd.HasFileSuffixSupport() {
			return "", makeNotDefinedError(vd)
		} else {
			// If a suffixed version is supported, try to load its value
			fileVarName := fmt.Sprintf("%s_%s", vd.Name, vd.FileSuffix)
			filename, found := os.LookupEnv(fileVarName)

			// If the suffixed version is not given either
			if !found {
				return "", makeNotDefinedError(vd)
			}
			b, err := ioutil.ReadFile(filename)
			if err != nil {
				return "", err
			}
			val := string(b)
			err = os.Setenv(vd.Name, val)
			if err != nil {
				return "", err
			}
		}
	}
	return os.Getenv(vd.Name), nil
}

func makeNotDefinedError(vd *VarDef) error {
	msg := fmt.Sprintf("environment variable required '%s'", vd.Name)
	if vd.HasFileSuffixSupport() {
		msg += fmt.Sprintf(" (can be provided from a file pointed by %s_%s)", vd.Name, vd.FileSuffix)
	}
	if vd.Description != "" {
		msg += ": " + vd.Description
	}
	return errors.New(msg)
}
