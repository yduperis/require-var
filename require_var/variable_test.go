package require_var

import (
	"os"
	"testing"
)

const (
	testFileVarFile                 string = "../tests/test_variable.txt"
	testVarName                     string = "TEST_VAR"
	testVarVal                      string = "test var"
	testFileVarName                 string = "TEST_F_VAR"
	testUndeclaredVarName           string = "TEST_UNDECLARED_VAR"
	testFileVarSuffix               string = "FILE"
	expectedTestFileSuffixedVarName string = "TEST_F_VAR_FILE"
	expectedTestFileVarVal          string = "This is a secret."
	testNotFoundFileVarName         string = "TEST_F_NOT_FOUND_VAR"
	testNotFoundFileVarFile         string = "../tests/not_existing_file"
	testNotFoundFileSuffixedVarName string = "TEST_F_NOT_FOUND_VAR_FILE"
)

func TestRequireVar(t *testing.T) {
	// Prepare variables
	err := os.Setenv(testVarName, testVarVal)
	if err != nil {
		panic(err)
	}
	err = os.Setenv(expectedTestFileSuffixedVarName, testFileVarFile)
	if err != nil {
		panic(err)
	}
	err = os.Setenv(testNotFoundFileSuffixedVarName, testNotFoundFileVarFile)
	if err != nil {
		panic(err)
	}

	val, err := RequireVar(nil)
	if err == nil {
		t.Error("Must have thrown error for nil VarDef")
	}

	undeclaredVarDef := &VarDef{
		Name:        testUndeclaredVarName,
		Description: "",
		FileSuffix:  "",
	}
	val, err = RequireVar(undeclaredVarDef)
	if err == nil {
		t.Error("Must have thrown error for undeclared variable without prefix")
	}

	undeclaredVarDef = &VarDef{
		Name:        testUndeclaredVarName,
		Description: "",
		FileSuffix:  testFileVarSuffix,
	}
	val, err = RequireVar(undeclaredVarDef)
	if err == nil {
		t.Error("Must have thrown error for undeclared variable with no prefixed variable declared as well")
	}

	noFileVarDef := &VarDef{
		Name:        testNotFoundFileVarName,
		Description: "",
		FileSuffix:  testFileVarSuffix,
	}
	val, err = RequireVar(noFileVarDef)
	if err == nil {
		t.Error("Must have thrown error for suffixed variable pointing to a non-existent file")
	}

	var1Def := &VarDef{
		Name:        testVarName,
		Description: "",
		FileSuffix:  "",
	}
	val, err = RequireVar(var1Def)
	if err != nil {
		t.Error("Must not throw error if required variable is declared")
	}
	if val != testVarVal {
		t.Error("Must have loaded correct file environment variable")
	}

	var2Def := &VarDef{
		Name:        testFileVarName,
		Description: "",
		FileSuffix:  testFileVarSuffix,
	}
	val, err = RequireVar(var2Def)
	if err != nil {
		t.Error("Must not throw error if required variable is declared through file env var")
	}
	if val != expectedTestFileVarVal {
		t.Error("Must have loaded correct file environment variable")
	}
}
