# require-var

Very simple command to enforce constraints on environment variables. Can also automatically load
environment variables from files if required. 

If you've ever written one of these lines in a `docker-entrypoint.sh`, this tool might help you:

1. Allow password reading from file
```shell script
if [ ! -z "$APP_DATABASE_PASSWORD_FILE" ]; then
    APP_DATABASE_PASSWORD=$(cat "$APP_DATABASE_PASSWORD_FILE")
fi
```

2. Print an error message if a required configuration variable is not provided
```shell script
if [ -z "$SOME_REQUIRED_CONFIG" ]; then
    echo "You must provide the server name with environment variable \$SOME_REQUIRED_CONFIG !"
    exit 1
fi
```

With `require-var`, you'd have written:

1. `require-var -from-file APP_DATABASE_PASSWORD`
2. `require-var SOME_REQUIRED_CONFIG`

## Installation

### In a go environment
`go get gitlab.com/yduperis/require-var`

### Everywhere else
Just download the [release](https://gitlab.com/yduperis/require-var/-/releases) for your platform, add it in a directory registered in your PATH,
make it executable and you're good to go !

### In a docker image
The main use case of this tool is inside of a Docker image entrypoint to provide the image user helpful error
message when the container hasn't been provided proper configuration. It is also a good tool to interface
with Docker Secrets.

For the last version, add these lines to your `Dockerfile`:

```dockerfile
[...]
WORKDIR /usr/local/bin
RUN wget https://gitlab.com/yduperis/require-var/uploads/6cb505dd0ea53bdb5ecf5a2452de704b/require-var \
    && chmod +x /usr/local/bin/require-var
[...]
```

## Usage

```shell script
require-var [-from-file] [-description DESCRIPTION] <ENVIRONMENT_VARIABLE_NAME>
```

Takes a variable definition and checks if the current environment meets the defined constraints.

The order of precedence for variable lookup is:
1. variable which name is given as argument
2. *_FILE* suffixed version of the variable (if the command is called with the `-from-file` flag)

If no value could be found, the function produces an informative error. If the variable description has
been provided with the flag `-description`, it will be printed to help the client developer to understand 
the error.

In the case of the `-from-file` scenario, if a _FILE suffixed version is found, the _FILE variable content 
will be loaded and filled in the provided variable name.

## With docker-compose and docker secrets
`myapp/docker-entrypoint.sh`
```shell script
DATABASE_PASSWORD=$(require-var -from-file DATABASE_PASSWORD)
```

`docker-compose.yml`
```yaml
[...]
secrets:
    app-database-password:
        external: true
[...]
services:
    myapp:
        image: myapp
        [...]
        environment:
          - DATABASE_PASSWORD_FILE=/run/secrets/app-database-password
        secrets:
          - app-database-password
        [...]
    [...]
[...]
```