package main

import (
	"bytes"
	"errors"
	"flag"
	"fmt"
	rv "gitlab.com/yduperis/require-var/require_var"
	"io"
	"os"
)

const (
	Version    string = "v1.1.0"
	FileSuffix string = "FILE"
)

func main() {
	help := flag.Bool(
		"h",
		false,
		"Displays help and exit")
	version := flag.Bool(
		"v",
		false,
		"Displays version and exit")
	description := flag.String(
		"description",
		"",
		"Description of the variable (displayed when the variable is not provided)")
	acceptFile := flag.Bool(
		"from-file",
		false,
		"Accept a variable suffixed by '_FILE' that points to a file containing the vale (useful for secrets)")

	flag.Parse()

	if *version {
		fmt.Printf("require-var version %s\n", Version)
		os.Exit(0)
	}
	if *help {
		fmt.Println(
			"Usage: require-var [-from-file] [-description DESCRIPTION] <ENVIRONMENT_VARIABLE_NAME>\n\n" +
				"This is a simple command that declares an environment variable as required.\n\n" +
				"It supports loading the variable from a file pointed by an environment variable suffixed by _FILE " +
				"(for example `require-var PASSWORD -from-file` will require a value for environment variable PASSWORD " +
				"and load the content of the file pointed by the variable PASSWORD_FILE in the variable PASSWORD if it has " +
				"no value).\n\n" +
				"The command will exit with an error if no value can be found for the variable or its file-suffixed version, " +
				"if -from-file flag is passed.")
		os.Exit(0)
	}

	if len(flag.Args()) < 1 {
		printError(errors.New("You must provide the environment variable name as argument"))
		os.Exit(1)
	}

	var varFileSufix string
	if *acceptFile {
		varFileSufix = FileSuffix
	}

	vName := flag.Args()[0]
	vDef := &rv.VarDef{
		Name:        vName,
		Description: *description,
		FileSuffix:  varFileSufix,
	}
	val, err := rv.RequireVar(vDef)
	if err != nil {
		printError(err)
		os.Exit(1)
	}
	_, _ = io.Copy(os.Stdout, bytes.NewBufferString(val))
	os.Exit(0)
}

func printError(err error) {
	_, _ = io.Copy(os.Stderr, bytes.NewBufferString(err.Error()+"\n"))
}
